﻿using System.Collections.Generic;
using System.Linq;

namespace Data
{
    internal static class Converter
    {
        internal static Person ToPerson(PersonData person, PersonPhoto personPhoto = null) =>
            new Person(new MultilingualName(person.FIRST_NAME_R, person.FIRST_NAME_U, person.FIRST_NAME_E),
                       new MultilingualName(person.LAST_NAME_R, person.LAST_NAME_U, person.LAST_NAME_E),
                       new MultilingualName(person.MIDDLE_NAME_R, person.MIDDLE_NAME_U, person.MIDDLE_NAME_E),
                       person.ID, person.OVD, person.CATEGORY, person.BIRTH_DATE, person.SEX,
                       person.LOST_DATE, person.LOST_DATE, person.ARTICLE_CRIM, person.RESTRAINT,
                       person.CONTACT , person.PHOTOID, personPhoto?.Vd, personPhoto?.Description, personPhoto?.PhotoBase64Encode);

        internal static List<Person> ToPersonList(List<PersonData> dataList, List<PersonPhoto> photoList)
        {
            var personList = new List<Person>();
            var photoDicti = photoList.ToDictionary(i => i.Id);

            for (int i = 0; i < dataList.Count; i++)
            {
                if (photoDicti.ContainsKey(dataList[i].PHOTOID))
                    personList.Add(Converter.ToPerson(dataList[i],photoDicti[dataList[i].PHOTOID]));
                else
                    personList.Add(Converter.ToPerson(dataList[i]));
            }
            return personList;
        }
    }
}
