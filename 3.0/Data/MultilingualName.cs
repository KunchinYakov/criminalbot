﻿namespace Data
{
    public class MultilingualName
    {
        public MultilingualName(string Name_R, string Name_U, string Name_E)
        {
            this.Name_R = Name_R;
            this.Name_U = Name_U;
            this.Name_E = Name_E;
        }
        public string Name_R{ get; set; }
        public string Name_U { get; set; }
        public string Name_E { get; set; }

        public static bool operator == (MultilingualName name1, string str)
        {
            str = str.ToUpper();
            if (name1.Name_E == str || name1.Name_R == str || name1.Name_U == str)
                return true;

            return false;
        }

        public static bool operator !=(MultilingualName name1, string str)
        {
            str = str.ToUpper();
            if (name1.Name_E == str || name1.Name_R == str || name1.Name_U == str)
                return false;

            return true;
        }

        public string GetRusName()
        {
            if (Name_R != "")
                return Name_R;

            else if (Name_U != "")
                return Name_U;

            else
                return Name_E;
        }

        public string GetEngName()
        {
            if (Name_R != "")
                return Name_E;

            else if (Name_U != "")
                return Name_R;

            else
                return Name_U;
        }
    }
}
