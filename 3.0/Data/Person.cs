﻿using Data;

namespace Data
{
    public class Person
    {
        public MultilingualName FirstName { get; set; }
        public MultilingualName LastName { get; set; }
        public MultilingualName MiddleName { get; set; }
        public string ID { get; set; }
        public string OVD { get; set; }
        public string CATEGORY { get; set; }
        public string BIRTH_DATE { get; set; }
        public string SEX { get; set; }
        public string LOST_DATE { get; set; }
        public string LOST_PLACE { get; set; }
        public string ARTICLE_CRIM { get; set; }
        public string RESTRAINT { get; set; }
        public string CONTACT { get; set; }
        public string PHOTOID { get; set; }
        public string Vd { get; set; }
        public string Description { get; set; }
        public byte[] Photo { get; set; }

        public Person(MultilingualName FirstName, MultilingualName LastName, MultilingualName MiddleName, 
                      string ID, string OVD, string CATEGORY, string BIRTH_DATE, string SEX, 
                      string LOST_DATE, string LOST_PLACE, string ARTICLE_CRIM, string RESTRAINT, 
                      string CONTACT, string PHOTOID, string Vd, string Description, byte[] photo)
        {
            this.ID = ID;
            this.OVD = OVD;
            this.CATEGORY = CATEGORY;
            this.BIRTH_DATE = BIRTH_DATE;
            this.SEX = SEX;
            this.LOST_DATE = LOST_DATE;
            this.LOST_PLACE = LOST_PLACE;
            this.ARTICLE_CRIM = ARTICLE_CRIM;
            this.RESTRAINT = RESTRAINT;
            this.CONTACT = CONTACT;
            this.PHOTOID = PHOTOID;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.MiddleName = MiddleName;
            this.Vd = Vd;
            this.Description = Description;
            this.Photo = photo;
        }
    }

}
