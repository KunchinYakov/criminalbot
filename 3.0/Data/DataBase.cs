﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Data
{
    public class DataBase
    {
        private DataBase()
        {
            List<PersonData> personData;
            List<PersonPhoto> personPhoto;
            try
            {
                personData = new ReadEmbeddedData<PersonData>("Data.EmbeddedData.Data.json")
                    .PersonData.Value.Result;
                personPhoto = new ReadEmbeddedData<PersonPhoto>("Data.EmbeddedData.mvswantedperson_photo_298.json")
                    .PersonData.Value.Result;
            }
            catch (FileNotFoundException)
            {
                personData = new List<PersonData>();
                personPhoto = new List<PersonPhoto>();
            }

            Persons = Converter.ToPersonList(personData, personPhoto);
        }
        public List<Person> Persons { get; set; }

        private static readonly Lazy<DataBase> instance =
            new Lazy<DataBase>(() => new DataBase());

        public  static DataBase GetInstance => instance.Value;
    }
}
