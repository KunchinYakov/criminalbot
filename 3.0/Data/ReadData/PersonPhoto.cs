﻿namespace Data
{
    internal class PersonPhoto
    {
        public string Id { get; set; }
        public string Vd { get; set; }
        public string Description { get; set; }
        public byte[] PhotoBase64Encode { get; set; }
    }
}
