﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Text.Json;
using System;
using System.Reflection;
using System.Reflection.Emit;

namespace Data
{
    internal class ReadEmbeddedData<T>
    {
        public ReadEmbeddedData(string Path)
        {
            this.Path = Path;
            PersonData = new Lazy<Task<List<T>>>(DataInitAsync, true);
        }

        internal Lazy<Task<List<T>>> PersonData { get; }

        private readonly string Path;

        private async Task<List<T>> DataInitAsync()
        {
            var assembly = Assembly.GetExecutingAssembly();

            using(var stream = assembly.GetManifestResourceStream(Path))
            {
                return await JsonSerializer.DeserializeAsync<List<T>>(stream);
            }
        }

    }
}
