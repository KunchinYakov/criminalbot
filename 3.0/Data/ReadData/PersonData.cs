﻿using System.Runtime.Serialization;

namespace Data
{
    [DataContract]
    internal class PersonData
    {
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public string OVD { get; set; }
        [DataMember]
        public string CATEGORY { get; set; }
        [DataMember]
        public string FIRST_NAME_R { get; set; }
        [DataMember]
        public string LAST_NAME_R { get; set; }
        [DataMember]
        public string MIDDLE_NAME_R { get; set; }
        [DataMember]
        public string BIRTH_DATE { get; set; }
        [DataMember]
        public string SEX { get; set; }
        [DataMember]
        public string LOST_DATE { get; set; }
        [DataMember]
        public string LOST_PLACE { get; set; }
        [DataMember]
        public string ARTICLE_CRIM { get; set; }
        [DataMember]
        public string RESTRAINT { get; set; }
        [DataMember]
        public string CONTACT { get; set; }
        [DataMember]
        public string PHOTOID { get; set; }
        [DataMember]
        public string FIRST_NAME_E { get; set; }
        [DataMember]
        public string LAST_NAME_E { get; set; }
        [DataMember]
        public string MIDDLE_NAME_E { get; set; }
        [DataMember]
        public string FIRST_NAME_U { get; set; }
        [DataMember]
        public string LAST_NAME_U { get; set; }
        [DataMember]
        public string MIDDLE_NAME_U { get; set; }
    }
}
