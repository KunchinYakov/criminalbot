﻿using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBot;

namespace TelegramBot
{
    public abstract class Command
    {
        public abstract  string Name { get; }
        public abstract ReplyKeyboardMarkup Keyboard { get; set; }

        public abstract void Execute(Message message, ITelegramBotClient client);

        protected virtual void ChangeState()
        {
            Bot.GetInstance.State.HandleMark(Mark.Neutral);
        }
        public virtual bool Contains(string command)
        {
            return command.Contains(this.Name);
        }
    }
}
