﻿namespace TelegramBot
{
    public static class CommandName
    {
        public const string StartCommand = @"/start";
        public const string ByNameCommand = @"By name";
        public const string ByBirthDateCommand = @"By birth date";
        public const string ByLostDateCommand = @"By lost date";
        public const string CancelCommand = @"Cancel";
        public const string SearchInDataBase = @"Search In Data Base";
    }
}
