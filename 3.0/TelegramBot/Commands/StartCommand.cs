﻿using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBot;

namespace TelegramBot
{
    public class StartCommand : Command
    {
        public StartCommand()
        {
            TelegramBot.KeyboardFactory factory = new SearchKeyboard();
            Keyboard = factory.Create();
        }
        public override string Name => CommandName.StartCommand;
        public override ReplyKeyboardMarkup Keyboard { get; set; }

        public override async void Execute(Message message, ITelegramBotClient client)
        {
            var chatId = message.Chat.Id;
            await client.SendTextMessageAsync(chatId, 
                                              "Welcome to СriminalBot. \n" +
                                              "I can find persons who are hiding from the government. \n" +
                                              "Search:", 
                                              replyMarkup: Keyboard);
            base.ChangeState();
        }
    }
}
