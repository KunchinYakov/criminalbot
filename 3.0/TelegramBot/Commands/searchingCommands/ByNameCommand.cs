﻿using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBot;

namespace TelegramBot
{
    public class ByNameCommand : Command
    {
        public ByNameCommand()
        {
            TelegramBot.KeyboardFactory factory = new CancelKeyboard();
            Keyboard = factory.Create();
        }
        public override string Name => CommandName.ByNameCommand;
        public override ReplyKeyboardMarkup Keyboard { get; set; }

        public override async void Execute(Message message, ITelegramBotClient client)
        {
            var chatId = message.Chat.Id;
            await client.SendTextMessageAsync(chatId, "Enter Name: ", replyMarkup: Keyboard);
            ChangeState();
        }

        protected override void ChangeState()
        {
            Bot.GetInstance.State.HandleMark(Mark.ByName);
        }
    }
}
