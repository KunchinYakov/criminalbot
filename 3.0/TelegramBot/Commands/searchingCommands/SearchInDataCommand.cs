﻿using Data;
using System.Collections.Generic;
using System.IO;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBot;

namespace TelegramBot
{
    public class SearchInDataCommand : Command
    {
        public SearchInDataCommand()
        {
            TelegramBot.KeyboardFactory factory = new CancelKeyboard();
            Keyboard = factory.Create();
        }
        public override string Name => CommandName.SearchInDataBase;
        public override ReplyKeyboardMarkup Keyboard { get; set; }

        public override void Execute(Message message, ITelegramBotClient client)
        {
            List<Person> printingData = Bot.GetInstance.State.Execute(message.Text);
            Send(message, client, printingData);
            // state the same
        }
        private async void Send(Message message, ITelegramBotClient client, IReadOnlyList<Person> returnList)
        {
            if (returnList.Count == 0)
            {
                await client.SendTextMessageAsync(message.Chat.Id, "0 results found", replyMarkup: Keyboard);
                return;
            }

            if (returnList.Count > 50)
            {
                await client.SendTextMessageAsync(message.Chat.Id, "too many results (over 50)", replyMarkup: Keyboard);
                return;
            }

            for (int i = 0; i < returnList.Count; i++)
            {
                string res = CreateEngResultStr(returnList[i]);

                if (returnList[i].Photo == null)
                    await client.SendTextMessageAsync(message.Chat.Id, res, replyMarkup: Keyboard); 
                else
                {
                    using (var stream = new MemoryStream(returnList[i].Photo))
                    {
                        var fileToSend = new InputOnlineFile(stream);
                        await client.SendPhotoAsync(message.Chat.Id, fileToSend, res);
                    }
                    
                }
            }
        }
        private string CreateUkrResultStr(Person person)
        {
            return "Прізвище: "    + person.FirstName.GetRusName() + '\n' +
                   "Ім'я: "        + person.LastName.GetRusName() + '\n' +
                   "По Батькові: " + person.MiddleName.GetRusName() + '\n' +
                   "Категорія: "   + person.CATEGORY + '\n' +
                   "Дата народження: " + person.BIRTH_DATE.Substring(0, 10) + '\n' +
                   "Затримання: "  + person.RESTRAINT + '\n'+
                   "Дата зникнення: " + person.LOST_DATE.Substring(0, 10) + '\n';
        }
        private string CreateEngResultStr(Person person)
        {
            return "First Name: " + person.FirstName.GetEngName() + '\n' +
                   "Last Name: " + person.LastName.GetEngName() + '\n' +
                   "Middle Name: " + person.MiddleName.GetEngName() + '\n' +
                   "Category : " + person.CATEGORY + '\n' +
                   "Birth Date: " + person.BIRTH_DATE.Substring(0, 10) + '\n' +
                   "Restraint: " + person.RESTRAINT + '\n' +
                   "Lost Date: " + person.LOST_DATE.Substring(0, 10) + '\n';
        }
        public override bool Contains(string command)
        {
            return true;
        }
    }
}