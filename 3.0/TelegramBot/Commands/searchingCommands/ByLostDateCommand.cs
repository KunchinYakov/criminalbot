﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBot;

namespace TelegramBot
{
    public class ByLostDateCommand:Command
    {
        public ByLostDateCommand()
        {
            TelegramBot.KeyboardFactory factory = new CancelKeyboard();
            Keyboard = factory.Create();
        }
        public override string Name => CommandName.ByLostDateCommand;
        public override ReplyKeyboardMarkup Keyboard { get; set; }
        public override async void Execute(Message message, ITelegramBotClient client)
        {
            var chatId = message.Chat.Id;
            await client.SendTextMessageAsync(chatId, "Format: YYYY-MM-DD", replyMarkup: Keyboard);
            ChangeState();
        }

        protected override void ChangeState()
        {
            Bot.GetInstance.State.HandleMark(Mark.ByLostDate);
        }
    }
}
