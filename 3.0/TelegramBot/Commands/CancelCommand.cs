﻿using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBot;

namespace TelegramBot
{
    class CancelCommand : Command
    {
        public CancelCommand()
        {
            TelegramBot.KeyboardFactory factory = new SearchKeyboard();
            Keyboard = factory.Create();
        }
        public override string Name => CommandName.CancelCommand;
        public override ReplyKeyboardMarkup Keyboard { get; set; }

        public override async void Execute(Message message, ITelegramBotClient client)
        {
            await client.SendTextMessageAsync(message.Chat.Id, "Default search: ", replyMarkup: Keyboard);
            base.ChangeState();
        }
    }
}
