﻿using System;
using System.Collections.Generic;
using Telegram.Bot;
using TelegramBot;

namespace TelegramBot
{
    public class Bot
    {
        private Bot()
        {
            State = new NeutralState();
        }
        private  static readonly Bot instance = new Bot();
        public static Bot GetInstance => instance;
        private TelegramBotClient client;
        private List<Command> commandsList;
        public IReadOnlyList<Command> commands => commandsList.AsReadOnly();
        public BotState State { get; set; }
        public TelegramBotClient Go()
        {
            if (client != null)
                return client;

            InitializeBot();

            client.StartReceiving();
            Console.WriteLine("working.... Press enter to exit. ");
            Console.ReadKey();
            client.StopReceiving();

            return client;
        }
        private void InitializeBot()
        {
            InitializeCommandsList();

            client = new TelegramBotClient(AppSettings.Key);
            client.OnMessage += Handler.Bot_OnMessage;
        }
        private void InitializeCommandsList()
        {
            commandsList = new List<Command>();
            commandsList.Add(new StartCommand());
            commandsList.Add(new ByNameCommand());
            commandsList.Add(new ByBirthDateCommand());
            commandsList.Add(new ByLostDateCommand());
            commandsList.Add(new CancelCommand());
            commandsList.Add(new SearchInDataCommand());//must be added last one
        }

        public void SaveExit()
        {
            if(client.IsReceiving)
                client.StopReceiving();
        }
    }
}
