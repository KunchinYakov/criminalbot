﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramBot
{
    public abstract class BotState
    {
        public virtual void HandleMark(Mark mark)
        {
            ChangeState(mark);
        }

        protected abstract void ChangeState(Mark mark);
        public abstract List<Person> Execute(string data);
    }
}
