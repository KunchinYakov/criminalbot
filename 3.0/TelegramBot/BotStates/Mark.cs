﻿namespace TelegramBot
{
    public enum Mark
    {
        Neutral,
        ByName,
        ByBirthDate,
        ByLostDate
    }
}