﻿using System;
using System.Collections.Generic;
using Data;

namespace TelegramBot
{
    public class ByLostDateState : BotState
    {
        protected override void ChangeState(Mark mark)
        {
            switch (mark)
            {
                case Mark.Neutral:
                    Bot.GetInstance.State = new NeutralState();
                    break;
                case Mark.ByLostDate:
                    break;
            }
        }

        public override List<Person> Execute(string data)
        {
            return Searcher.SearchByLostDate(data);
        }
    }
}