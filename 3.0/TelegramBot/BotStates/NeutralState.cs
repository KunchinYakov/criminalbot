﻿using System;
using System.Collections.Generic;
using Data;

namespace TelegramBot
{
    public class NeutralState : BotState
    {
        protected override void ChangeState(Mark mark)
        {
            switch (mark)
            {
                case Mark.ByName:
                    Bot.GetInstance.State = new ByNameState();
                    break;
                case Mark.ByBirthDate:
                    Bot.GetInstance.State = new ByBirthDateState();
                    break;
                case Mark.ByLostDate:
                    Bot.GetInstance.State = new ByLostDateState();
                    break;
                case Mark.Neutral:
                    break;
            }
        }

        public override List<Person> Execute(string data)
        {
            return Searcher.DefaultSearch(data);
        }
    }
}