﻿using System.Text.RegularExpressions;

namespace TelegramBot
{
    public static class Parser
    {
        /// <summary>
        /// What's in the str?
        /// </summary>
        /// <returns> str is 1 word, 2 words, 3 words or date</returns>
        public static string Parse(string str)
        {
            if (IsDate(str))
                return "date";

            return CountWords(str);
        }
        private static string CountWords(string str)
        {
            var regex = new Regex(@"[^а-яА-Яa-zA-Z 'єЄґҐеЕіІ]+");//consists of letters only
            if (regex.IsMatch(str))
                return "Error";
            return str.Split(' ').Length.ToString() + " words";
        }
        private static bool IsDate(string str)
        {
            var regex = new Regex(@"\d{4}-\d{2}-\d{2}");//Format: YYYY-MM-DD

            if (regex.IsMatch(str))
                return true;

            return false;
        }
    }
}
