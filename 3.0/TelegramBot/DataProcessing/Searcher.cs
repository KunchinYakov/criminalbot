﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data;

namespace TelegramBot
{
    public static class Searcher
    {
        private static DataBase dataBase = DataBase.GetInstance;

        public static List<Person> DefaultSearch(string text)
        {
            string textType = Parser.Parse(text);

            switch (textType)
            {
                case "date":
                    return SearchByBirthDate(text);

                case "1 words":
                    return SearchByOneWord(text);

                case "2 words":
                    return SearchByTwoWords(text);

                case "3 words":
                    return SearchByTreeWords(text);

                default: 
                    return new List<Person>();
            }
        }

        public static List<Person> SearchByBirthDate(string birthDate)
        {
            return SearchByDate(birthDate, (person => person.BIRTH_DATE));
        }

        public static List<Person> SearchByLostDate(string birthDate)
        {
            return SearchByDate(birthDate, (person => person.LOST_DATE));
        }

        private static List<Person> SearchByDate(string date, Func<Person, string> option)
        {
            string textType = Parser.Parse(date);

            string format = "T00:00:00";
            switch (textType)
            {
                case "date":
                    return dataBase.Persons.Where(t => option(t) == date + format).ToList();
                default:
                    return new List<Person>();
            }
        }

        public static List<Person> SearchByName(string text)
        {
            string textType = Parser.Parse(text);

            switch (textType)
            {
                case "1 words":
                    return SearchByOneWord(text);

                case "2 words":
                    return SearchByTwoWords(text);

                case "3 words":
                    return SearchByTreeWords(text);

                default: 
                    return new List<Person>();
            }
        }

        private static List<Person> SearchByOneWord(string word)
        {
            var replyList = new List<Person>();
            for (int i = 0; i < dataBase.Persons.Count; i++)
            {
                if (dataBase.Persons[i].FirstName == word || 
                    dataBase.Persons[i].LastName == word || 
                    dataBase.Persons[i].MiddleName == word)
                {
                    replyList.Add(dataBase.Persons[i]);
                }
            }

            return replyList;
        }

        private static List<Person> SearchByTwoWords(string twoWords)
        {
            string[] words = twoWords.Split(' ');
            var replyList = new List<Person>();
            for (int i = 0; i < dataBase.Persons.Count; i++)
            {
                var person = dataBase.Persons[i];

                //if words consist of LastName + MiddleName
                if (person.LastName == words[0] && person.MiddleName == words[1])
                    replyList.Add(dataBase.Persons[i]);

                //if words consist of FirstName + LastName
                else if (person.FirstName == words[0] && person.LastName == words[1])
                    replyList.Add(dataBase.Persons[i]);

                //if words consist of LastName + FirstName
                else if (person.LastName == words[0] && person.FirstName == words[1])
                    replyList.Add(dataBase.Persons[i]);
            }

            return replyList;
        }

        private static List<Person> SearchByTreeWords(string treeWords)
        {
            string[] words = treeWords.Split(' ');
            var replyList = new List<Person>();
            for (int i = 0; i < dataBase.Persons.Count; i++)
            {
                var person = dataBase.Persons[i];
                if (person.FirstName == words[0] && person.LastName == words[1] && person.MiddleName == words[2])
                {
                    replyList.Add(dataBase.Persons[i]);
                }
            }

            return replyList;
        }
    }

}
