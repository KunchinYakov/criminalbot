﻿using Telegram.Bot.Types.ReplyMarkups;
using TelegramBot;

namespace TelegramBot
{
    public class SearchKeyboard : KeyboardFactory
    {
        public override ReplyKeyboardMarkup Create()
        {
            return Keyboard =
                new ReplyKeyboardMarkup(new[]
                {
                    new[]
                    {
                        new KeyboardButton(CommandName.ByNameCommand),
                        new KeyboardButton(CommandName.ByBirthDateCommand),
                        new KeyboardButton(CommandName.ByLostDateCommand)
                    }
                }, true);
        }
    }
}