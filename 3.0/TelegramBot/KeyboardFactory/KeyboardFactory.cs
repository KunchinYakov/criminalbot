﻿using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramBot
{
    public abstract class KeyboardFactory
    {
        public ReplyKeyboardMarkup Keyboard { get; set; }

        public abstract ReplyKeyboardMarkup Create();

    }
}
