﻿using Telegram.Bot.Types.ReplyMarkups;
using TelegramBot;

namespace TelegramBot
{
    public class CancelKeyboard: KeyboardFactory
    {
        public override ReplyKeyboardMarkup Create()
        {
            return Keyboard =
                new ReplyKeyboardMarkup(new[]
                {
                    new KeyboardButton(CommandName.CancelCommand)
                }, true);
        }
    }
}
