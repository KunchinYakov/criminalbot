﻿using Telegram.Bot.Args;

namespace TelegramBot
{
    public static class Handler
    {
        public static void Bot_OnMessage(object sender, MessageEventArgs e)
        {
            var message = e.Message;
            var text = e.Message.Text;
            foreach (var command in Bot.GetInstance.commands)
            {
                if (command.Contains(text))
                {
                    command.Execute(message, Bot.GetInstance.Go());
                    return;
                }
            }
        }
    }
}
