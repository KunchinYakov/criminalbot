﻿using System.Text.RegularExpressions;
using Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TelegramBot;

namespace TelegramBotUnitTest
{
    [TestClass]
    public class UnitTestParser
    {
        [TestMethod]
        public void TestIsDate1()
        {
            Assert.AreEqual("date", Parser.Parse("1000-10-10"));
        }
        [TestMethod]
        public void TestIsDate2()
        {
            Assert.AreEqual("Error", Parser.Parse("100-10-10"));
        }
        [TestMethod]
        public void TestIsDate3()
        {
            Assert.AreEqual("Error", Parser.Parse("1000-1-10"));
        }
        [TestMethod]
        public void TestIsDate4()
        {
            Assert.AreEqual("Error", Parser.Parse("1000-10-1"));
        }
        [TestMethod]
        public void TestIsDate5()
        {
            Assert.AreEqual("Error", Parser.Parse("1000.10-10"));
        }
        [TestMethod]
        public void TestIsDate6()
        {
            Assert.AreEqual("Error", Parser.Parse("1000-10.10"));
        }
        [TestMethod]
        public void TestCountWords1()
        {
            Assert.AreEqual("1 words", Parser.Parse("word"));
        }
        [TestMethod]
        public void TestCountWords2()
        {
            Assert.AreEqual("2 words", Parser.Parse("word word"));
        }
        [TestMethod]
        public void TestCountWords3()
        {
            Assert.AreEqual("3 words", Parser.Parse("word word word"));
        }

        [TestMethod]
        public void TestCountWords4()
        {
            Assert.AreEqual("Error", Parser.Parse("000"));
        }
        [TestMethod]
        public void TestCountWords5()
        {
            Assert.AreEqual("Error", Parser.Parse("0 00"));
        }
        [TestMethod]
        public void TestCountWords6()
        {
            Assert.AreEqual("Error", Parser.Parse("0 0 0"));
        }

        [TestMethod]
        public void TestCountWords7()
        {
            Assert.AreEqual("Error", Parser.Parse("word 0"));
        }
        [TestMethod]
        public void TestCountWords8()
        {
            Assert.AreEqual("Error", Parser.Parse("0 word"));
        }
        [TestMethod]
        public void TestCountWords9()
        {
            Assert.AreEqual("Error", Parser.Parse("word word 0"));
        }
        [TestMethod]
        public void TestCountWords10()
        {
            Assert.AreEqual("Error", Parser.Parse("0 word word"));
        }
        [TestMethod]
        public void TestCountWords11()
        {
            Assert.AreEqual("Error", Parser.Parse("word 0 word"));
        }
        [TestMethod]
        public void TestCountWords12()
        {
            Assert.AreEqual("Error", Parser.Parse(". word"));
        }
        [TestMethod]
        public void TestCountWords13()
        {
            Assert.AreEqual("Error", Parser.Parse("word ."));
        }
        public void TestCountWords14()
        {
            Assert.AreEqual("Error", Parser.Parse(". word word"));
        }
        [TestMethod]
        public void TestCountWords15()
        {
            Assert.AreEqual("Error", Parser.Parse("word . word"));
        }
        [TestMethod]
        public void TestCountWords16()
        {
            Assert.AreEqual("Error", Parser.Parse("word word ."));
        }
    }

    
}

